const path = require('path');
const HtmlPlugin = require('html-webpack-plugin');
const BabiliWebpackPlugin = require('babili-webpack-plugin')
const CopyWebpackPlugin = require('copy-webpack-plugin')
const ExtractTextPlugin = require('extract-text-webpack-plugin')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const UglifyJsPlugin = require('uglifyjs-webpack-plugin')
const webpack = require('webpack');

const mode = process.env.NODE_ENV == 'production' ? 'production' : 'development';

module.exports = {
    mode: mode,
    entry: {
      'index': './src/controller/index.js',
      'reward_develop_projects': './src/controller/reward_develop_projects.js',
      'threejs': './src/components/threejs/three.min.js'
    },
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: '[name][hash].js'
    },
    module: {
        rules: [
          {
            test: /\.css$/,
            use: ExtractTextPlugin.extract({
              fallback: 'style-loader',
              use: [
                {
                    loader: 'css-loader',
                    options: {
                        url: true,
                        minimize: true,
                        sourceMap: false
                    }
                }
            ]
            })
          },
          {
            test: /\.html$/,
            use: 'vue-html-loader'
          },
          {
            test: /\.js$/,
            use: 'babel-loader',
            exclude: /node_modules/
          },
          {
            test: /\.(png|jpe?g|gif|svg)(\?.*)?$/,
            use: {
              loader: 'url-loader',
              query: {
                limit: 10000,
                name: '[name].[ext]'
              }
            }
          },
          {
            test: /\.(ico)(\?.*)?$/,
            use: {
              loader: 'url-loader',
              query: {
                limit: 100,
                name: '[name].[ext]'
              }
            }
          },
          {
            test: /\.(mp4|webm|ogg|mp3|wav|flac|aac)(\?.*)?$/,
            loader: 'url-loader',
            options: {
              limit: 10000,
              name: 'media/[name]--[folder].[ext]'
            }
          },
          {
            test: /\.(woff2?|eot|ttf|otf)(\?.*)?$/,
            use: {
              loader: 'url-loader',
              query: {
                limit: 10000,
                name: 'fonts/[name]--[folder].[ext]'
              }
            }
          }
        ]
    },

    devServer: {
        // proxy: { 
        //   '/web': 'http://127.0.0.1:3003'
        // },
        contentBase: path.join(__dirname, 'dist'), 
        hot: true, 
        host: '0.0.0.0',
        port: 8089,
        open: true
    },

    plugins: [
        new HtmlPlugin({
          chunks: ['index'],
          template: path.resolve(__dirname, 'src/html/index.html'),
          inject: true,
          filename: 'index.html',
        }),
        new HtmlPlugin({
          chunks: ['reward_develop_projects'],
          template: path.resolve(__dirname, 'src/html/reward_develop_projects.html'),
          inject: true,
          filename: 'reward_develop_projects.html',
        }),
        new UglifyJsPlugin({
          uglifyOptions: {
            compress: true,
            output: {
              comments: false,
              beautify: true
            }
          }
        }),
         new ExtractTextPlugin('[name]_.[hash].css', {
            disable: false,
            allChunks: true
        }),
         new webpack.ProvidePlugin({
            'THREE': 'three'
          }),
    ],

    resolve: {
      modules: [
        path.resolve(__dirname, "app"), "node_modules"
      ],
      alias: {
        'three$': 'three/build/three.min.js',
        'three/.*$': 'three',
      }
    }
	
}