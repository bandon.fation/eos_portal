import '../assets/css/index.css'
import '../assets/css/mobile.css'
import '../assets/images/wx-link.jpg'
import $ from 'jquery'
const call_lang_module = async (lang_key) => {
    let en = await import('../components/lang/' + lang_key + '.js');
    return en.default;
}
import '../assets/images/favicon.ico'
;(function(){
    $('[page_item]').eq(0).height(document.documentElement.clientHeight - 76);
    $('body').css({'opacity': 1});
})();
var lang = navigator.language || navigator.userLanguage;
lang = lang.substr(0, 2);
var key_map = {
    zh: 'cn',
    cn: 'cn',
    en: 'en',
    ko: 'kr',
    kr: 'kr',
}
var key = (window.location.href.split('lang=')[1]||'').toLowerCase()
key = key ? key : lang;
key = key_map[key] ? key_map[key] : 'en';
call_lang_module(key)
.then(lang => {
    key_map[key] = lang;
    $('[v-bind]').each(function(index, el){
    var attrs = $(el).attr('v-bind').split(';');
    $(attrs).each(function(i, v){
            var val = v.split(':');
            if(val[0] == 'text'){
                el.innerText = key_map[key][val[1]];
            }else if(val[0] == 'innerHTML'){
                el.innerHTML = key_map[key][val[1]];
            }else{
                $(el).attr(val[0], key_map[key][val[1]]);
            }
        })
    });
    $('[nav_click="hide"]').on('click', function(){
        $('[mobile_head_nav]').hide();
    });
    $('[nav_click="show"]').on('click', function(){
        $('[mobile_head_nav]').show();
    });
    $('[mobile_head_nav]').on('click', function(e){
        let target = e.target || e.srcElement;
        if(target === this){
            $(this).hide();
        }
    });
});