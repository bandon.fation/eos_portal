# _*_ coding: utf-8 _*_
import pandas as pd
import numpy as np


import xlrd
import csv

filename = "./data_2019_01_15.csv"
csv_data = pd.read_csv(filename, header=None)
records = csv_data.to_records()
html = ''
for index in range(len(records)):
    if index < 1:
        continue 
    row = records[index]
    # print(row)
    row = list(row)
    # print(row)
    date_cols = row[4].split('/')
    # print(date_cols)
    # year
    date_cols[2] = '20' + date_cols[2]
    # day
    date_cols[1] = '0' + date_cols[1] if len(date_cols[1]) < 2 else date_cols[1]
    # month
    date_cols[0] = '0' + date_cols[0] if len(date_cols[0]) < 2 else date_cols[0]

    row[4] = date_cols[2] + '-' + date_cols[0] + '-' + date_cols[1]

    _class_name = 'hide' if index > 3 else ''
    row.append(_class_name)
    html += '''<li  class="{6}">
        <a href="{5}" target="_blank">
            【{3}】{2}
        </a>
        <span class="media_time">{4}</span>
    </li>
    '''.format(*row)

print(html)
